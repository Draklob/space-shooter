﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour {

	public float dodge;
	public float smoothing;
	public float tilt;
	public Vector2 startWait;
	public Vector2 maneuverTime;
	public Vector2 maneuverWait;
	public Boundary boundary;

	private Transform playerTransform;
	private float currentSpeed;
	private float targetManeuver;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		// Check if player still exists.
		if( GameObject.FindGameObjectWithTag( "Player" ) )
			playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		
		rb = GetComponent<Rigidbody>();
		currentSpeed = rb.velocity.z;
			StartCoroutine( Evade () );
	}
	
	// Set a time to move and stop the movement of the airships.
	IEnumerator Evade()
	{
		yield return new WaitForSeconds( Random.Range( startWait.x, startWait.y ) );

		while( true)
		{
			if( playerTransform != null )
				
				targetManeuver = playerTransform.position.x;
			else
				targetManeuver = Random.Range( 1, dodge * -Mathf.Sign( transform.position.x ));
			yield return new WaitForSeconds( Random.Range ( maneuverTime.x, maneuverTime.y ));
			targetManeuver = 0;
			yield return new WaitForSeconds( Random.Range( maneuverWait.x, maneuverWait.y ));
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		// Calculate how much it moves the airship in X.
		float newManeuver = Mathf.MoveTowards( rb.velocity.x, targetManeuver, Time.deltaTime * smoothing );
		// Apply movement to the airships in the axis X and Z.
		rb.velocity = new Vector3( newManeuver, 0.0f, currentSpeed );
		
		// We make sure the airships don't get out of the boundary.
		rb.position = new Vector3
		(
			Mathf.Clamp( rb.position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp( rb.position.z, boundary.zMin, boundary.zMax )
		);

		// Apply rotation to the airships when they change of position.
		rb.rotation = Quaternion.Euler( 0.0f, 0.0f, rb.velocity.x * -tilt );
	}
}
