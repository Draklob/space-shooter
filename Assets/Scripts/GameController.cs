﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject [] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public Text scoreText;
	private int score;

	private void Start()
	{
		StartCoroutine( SpawnWaves());
		score = 0;
		UpdateScore();
	}

	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds( startWait );

		while( true )
		{
			for( int i = 0; i < hazardCount; i++ )
			{
				GameObject hazard = hazards[Random.Range(0, hazards.Length)];
				Vector3 spawnPosition = new Vector3( Random.Range( -spawnValues.x, spawnValues.x ), spawnValues.y, spawnValues.z );
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate( hazard, spawnPosition, spawnRotation );
				yield return new WaitForSeconds( spawnWait );
			}
		}
	}

	public void AddScore( int newScore )
	{
		score += newScore;
		UpdateScore();
	}

	void UpdateScore()
	{
		scoreText.text = "Score " + score;
	}
}
