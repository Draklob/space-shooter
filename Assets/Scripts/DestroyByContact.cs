﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;

	private GameController gameController;

	private void Start()
	{
		GameObject gameControllerObj = GameObject.FindWithTag("GameController");
		if( gameControllerObj != null )
			gameController = gameControllerObj.GetComponent<GameController>();
	}

	private void OnTriggerEnter( Collider other )
	{
		if( other.tag == "Boundary" || other.tag == "Enemy" )
			return;

		if( other.tag == "Player" )
		{
			Instantiate( playerExplosion, other.transform.position, other.transform.rotation );
		}

		Destroy( other.gameObject);
		gameController.AddScore( scoreValue );
		if( explosion != null)
			Instantiate( explosion, transform.position, transform.rotation );
		Destroy( gameObject ); 
	}
}
